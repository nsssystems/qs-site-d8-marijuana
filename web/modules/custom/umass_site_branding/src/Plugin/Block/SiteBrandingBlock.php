<?php

namespace Drupal\umass_site_branding\Plugin\Block;

use Drupal\Core\Block\BlockBase;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'UMass Site Branding' Block.
 *
 * @Block(
 *   id = "umass_site_branding_block",
 *   admin_label = @Translation("UMass Site Branding"),
 *   category = @Translation("UMass"),
 * )
 */
class SiteBrandingBlock extends BlockBase {
    /**
     * {@inheritdoc}
     */
    public function build() {

        return [
            '#theme' => 'umass_site_branding',
            // 'abc' => 12345,
            // 'def' => 'duck',
        ];
   }

/**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();

	$form['umass_site_branding'] = array(
		'#type' => 'fieldset',
		'#title' => t('UMass Site Branding'),
		'#description' => t('Block display options for UMass site branding. Reference <a href="https://www.umass.edu/brand/brand-architecture/web-brand-guidelines">UMass web brand guidelines</a>.'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE
	);
	$form['umass_site_branding']['umass_site_branding_option_1'] = array(
		'#type' => 'checkbox',
		'#title' => t('Maroon'),
		'#default_value' => isset($config['umass_site_branding_option_1']) ? $config['umass_site_branding_option_1'] : 0,
		'#description' => t('Select to include CSS class in site name.')
	);
	$form['umass_site_branding']['umass_site_branding_option_2'] = array(
		'#type' => 'checkbox',
		'#title' => t('Serif'),
		'#default_value' => isset($config['umass_site_branding_option_2']) ? $config['umass_site_branding_option_2'] : 0,
		'#description' => t('Select to include CSS class in site name.')
	);
	$form['umass_site_branding']['umass_site_branding_option_3'] = array(
		'#type' => 'checkbox',
		'#title' => t('Reversed'),
		'#default_value' => isset($config['umass_site_branding_option_3']) ? $config['umass_site_branding_option_3'] : 0,
		'#description' => t('Select to include CSS class in site name and parent site name.')
	);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Save our custom settings when the form is submitted.
    // dpm($form_state);
    $this->setConfigurationValue('umass_site_branding_option_1', $form_state->getValue(array('umass_site_branding','umass_site_branding_option_1')));
    $this->setConfigurationValue('umass_site_branding_option_2', $form_state->getValue(array('umass_site_branding','umass_site_branding_option_2')));
    $this->setConfigurationValue('umass_site_branding_option_3', $form_state->getValue(array('umass_site_branding','umass_site_branding_option_3')));
  }

  /**
   * {@inheritdoc}
   */
  // public function blockValidate($form, FormStateInterface $form_state) {
  //   $umass_site_branding_option_1 = $form_state->getValue('umass_site_branding_option_1');

  //   if (!is_numeric($umass_site_branding_option_1)) {
  //     $form_state->setErrorByName('umass_site_branding_option_1', t('It Needs to be an integer') . ':'. $umass_site_branding_option_1);
  //   }
  // }

}