<?php

/**
 * @file
 * 
 * Contains umass_global_branding.module.
 *
 * UMass Global Branding module swaps in a global header and footer from external markup, css, and js resources.
 *
 * To enable DEBUG mode from and find resources at dev.www.umass.edu instead:
 *    drush cset umass.site_branding umass_branding_debug 1
 * Remove the variable for production use
 *    drush cdel umass.site_branding umass_branding_debug
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Cache\CacheBackendInterface;

// Configuration
// Static external resource files are located here at www or at dev.www depending on debug
define('UMASS_BRANDING_RESOURCE_BASE', 'https://'.(\Drupal::config('umass.site_branding')->get('umass_branding_debug') ? 'dev.' : '').'www.umass.edu/static/branding/');
// define('UMASS_BRANDING_RESOURCE_BASE', 'https://www.umass.edu/static/branding/');
define('UMASS_BRANDING_RESOURCE_HEADER_HTML', UMASS_BRANDING_RESOURCE_BASE.'umass_header.html');
define('UMASS_BRANDING_RESOURCE_FOOTER_HTML', UMASS_BRANDING_RESOURCE_BASE.'umass_footer.html');
// Static external JS and CSS resource locations are set in file umass_global_branding.libraries.yml

// cache key base
define('UMASS_BRANDING_CACHE_BASE', 'umass_global_branding/');

/**
 * Implements hook_help().
 */
function umass_global_branding_help($route_name, \Drupal\Core\Routing\RouteMatchInterface $route_match) {
  switch ($route_name) {

    case 'help.page.umass_global_branding':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Displays UMass Amherst global header and footer') . '</p>';
      return $output;
    }
}

/**
 * Fetch and cache global umass_global_branding assets
 */
function _umass_global_branding_data ($resource) {

  $text = NULL;
  $url  = NULL;

  // check cache for resource
  if ($cache = \Drupal::cache()->get(UMASS_BRANDING_CACHE_BASE.$resource)) {
    // $message = t('Found in cache!: %resource',['%resource' => $resource]);
    // \Drupal::logger('umass_global_branding')->notice($message);
    $text =  $cache->data;
  }

  if (empty($text)) {

    switch ($resource) {
      case 'header_html':
        $url = UMASS_BRANDING_RESOURCE_HEADER_HTML;
        break;
      case 'footer_html':
        $url = UMASS_BRANDING_RESOURCE_FOOTER_HTML;
        break;
    }

    // fetch and cache resource
    try {
      $response = \Drupal::httpClient()->get($url);
      $expire = time() + 24*60*60; // one day later

      switch ($response->getStatusCode()) {
        case 200:
          $text = (string) $response->getBody();

          // in footer only, replace slugs with site year and email address
          if ($resource == 'footer_html'){
            $search = array('%UMASS_SITE_YEAR', '%UMASS_SITE_EMAIL');
            $site_year = date('Y');
            $site_email = \Drupal::config('system.site')->get('mail');
            $replace = array($site_year, $site_email?$site_email:'');
            $text = str_replace($search, $replace, $text);
          }

          \Drupal::cache()->set(UMASS_BRANDING_CACHE_BASE.$resource, $text, $expire);
          // $message = t('Set external resource in cache. HTTP code: %code : %resource', ['%resource' => $resource, '%code' =>$response->getStatusCode()]);
          // \Drupal::logger('umass_global_branding')->notice($message);
          break;
        case 301:
        case 302:
          $text = (string) $response->getBody();
          \Drupal::cache()->set(UMASS_BRANDING_CACHE_BASE.$resource, $text, $expire);
          $message = t('Redirect to external resource was cached. HTTP code: %code : %resource', ['%resource' => $resource, '%code' =>$response->getStatusCode()]);
          \Drupal::logger('umass_global_branding')->notice($message);
          break;
        
        default:
          $message = t('Bad external resource. Cache not set. HTTP code: %code : %resource', ['%resource' => $resource, '%code' =>$response->getStatusCode()]);
          \Drupal::logger('umass_global_branding')->error($message);
          break;
      }
    }
    catch (Exception $e) {
      \Drupal::logger('umass_global_branding')->error($e->getMessage());

      return FALSE;
    }
  }

  return $text;
}

/**
 * Implements hook_page_attachments().
 */
function umass_global_branding_page_attachments(array &$attachments) {
  $route = \Drupal::routeMatch()->getRouteObject();
  $is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);

  // not an admin page
  if (!$is_admin) {
    // not using debug resources
    if (!(\Drupal::config('umass.site_branding')->get('umass_branding_debug'))) {
      $attachments['#attached']['library'][] = 'umass_global_branding/css';
      $attachments['#attached']['library'][] = 'umass_global_branding/js';
    } else {
      $attachments['#attached']['library'][] = 'umass_global_branding/css_dev';
      $attachments['#attached']['library'][] = 'umass_global_branding/js_dev';
    }
  }
}

/**
 * Implements hook_page_top().
 */
function umass_global_branding_page_top(array &$page_top) {
  $route = \Drupal::routeMatch()->getRouteObject();
  $is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);

  if (!$is_admin) {
    $markup = _umass_global_branding_data('header_html');

    $page_top['umass_global_branding'] = [
      '#type' => 'inline_template',
      '#template' => '{{ markup | raw }}',
      '#context' => [
        'markup' => $markup
      ]
    ];
  }
}

/**
 * Implements hook_page_bottom().
 */
function umass_global_branding_page_bottom(array &$page_bottom) {
  $route = \Drupal::routeMatch()->getRouteObject();
  $is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);

  if (!$is_admin) {
    $markup = _umass_global_branding_data('footer_html');

    $page_bottom['umass_footer'] = [
      '#type' => 'inline_template',
      '#template' => '{{ markup | raw }}',
      '#context' => [
        'markup' => $markup
      ]
    ];
  }
}
